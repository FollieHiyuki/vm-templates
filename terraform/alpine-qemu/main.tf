terraform {
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.14"
    }
  }
}

variable "vm_name" {
  type        = string
  default     = "packer-alpine-edge-qemu-x86_64"
  description = "Name of the VM to import"
}

variable "running" {
  type        = bool
  nullable    = false
  default     = true
  description = "Dynamically start/stop the VM"
}

variable "cpus" {
  type        = number
  nullable    = false
  default     = 2
  description = "Number of CPU cores to use for the VM"
}

variable "ram" {
  type        = string
  nullable    = false
  default     = "2048"
  description = "The amount of memory assigned to the VM"
}

provider "libvirt" {
  uri = "qemu:///system"
}

resource "libvirt_volume" "alpine-qemu-image" {
  name   = "${var.vm_name}.qcow2"
  source = "../../artifacts/qemu/${var.vm_name}.qcow2"
  pool   = "default"
}

resource "libvirt_domain" "alpine-qemu" {
  name       = var.vm_name
  vcpu       = var.cpus
  memory     = var.ram
  running    = var.running
  qemu_agent = true
  autostart  = false

  disk {
    volume_id = libvirt_volume.alpine-qemu-image.id
  }

  network_interface {
    network_name   = "default"
    wait_for_lease = true
  }
}

output "ips" {
  value = libvirt_domain.alpine-qemu.network_interface.0.addresses
}
