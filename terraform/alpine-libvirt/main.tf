terraform {
  required_providers {
    libvirt = {
      source  = "dmacvicar/libvirt"
      version = "0.6.14"
    }
  }
}

variable "vm_name" {
  type        = string
  default     = "packer-alpine-stable-uefi-libvirt-x86_64"
  description = "Name of the VM to import"
}

variable "running" {
  type        = bool
  nullable    = false
  default     = true
  description = "Dynamically start/stop the VM"
}

variable "cpus" {
  type        = number
  nullable    = false
  default     = 2
  description = "Number of CPU cores to use for the VM"
}

variable "ram" {
  type        = string
  nullable    = false
  default     = "2048"
  description = "The amount of memory assigned to the VM"
}

provider "libvirt" {
  uri = "qemu:///system"
}

resource "libvirt_volume" "alpine-qemu-image" {
  name   = "${var.vm_name}.qcow2"
  source = "../../artifacts/libvirt/${var.vm_name}.qcow2"
  pool   = "default"
}

resource "libvirt_domain" "alpine-libvirt" {
  name       = var.vm_name
  vcpu       = var.cpus
  memory     = var.ram
  running    = var.running
  qemu_agent = true
  autostart  = false
  firmware   = "/usr/share/OVMF/OVMF.fd"

  # We already built the image with host's CPU features
  cpu {
    mode = "host-passthrough"
  }

  disk {
    volume_id = libvirt_volume.alpine-qemu-image.id
  }

  network_interface {
    network_name   = "default"
    wait_for_lease = true
  }

  # For usage with virt-manager GUI
  console {
    type        = "pty"
    target_port = "0"
    target_type = "virtio"
    source_path = "/dev/pts/1"
  }
}

output "ips" {
  value = libvirt_domain.alpine-libvirt.network_interface.0.addresses
}
