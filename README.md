# Packer templates

This repository holds Packer's VM templates I use. Most (if not all) of them run [AlpineLinux](https://alpinelinux.org).

## Dependencies

For [update_iso_checksum.sh](./update_iso_checksum.sh) script:
- [hcl2json](https://github.com/tmccombs/hcl2json)
- [jq](https://github.com/stedolan/jq) ([gojq](https://github.com/itchyny/gojq) and [jaq](https://github.com/01mf02/jaq) also work)

Other:
- [packer](https://www.packer.io/) (of course)
- [terraform](https://www.terraform.io/)
- [make](https://www.gnu.org/software/make/) (for convenience’ sake)
- [qemu](http://www.qemu.org/) + [libvirt](https://libvirt.org/)

Note that libvirt plugins (for both Packer and Terraform) do not support modular daemons so running `libvirtd` service is required. Issue filled at <https://github.com/digitalocean/go-libvirt/issues/171>.

## VM boxes

- [alpine-qemu.pkr.hcl](./alpine-qemu.pkr.hcl): libvirt-compatible VM on local machine running Alpine edge

```bash
VM_STATE_RUNNING=true make alpine-qemu.pkr.hcl alpine-qemu
```

- [alpine-libvirt.pkr.hcl](./alpine-libvirt.pkr.hcl): a variation of `alpine-qemu` box with UEFI and stable APK repository

```bash
make alpine-libvirt.pkr.hcl alpine-libvirt
```

## Acknowledge

- [rgl/alpine-vagrant](https://github.com/rgl/alpine-vagrant)

## License

MIT
